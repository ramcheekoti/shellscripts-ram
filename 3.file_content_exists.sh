#!/bin/bash
file=$1
if [ -e "${file}" ]
then
 echo -e "$1 is present"
    if [ -s "${file}" ]
      then 
      echo -e "$1 has content"
      else
      echo -e "$1 does not have content"
    fi
else 
echo -e "$1 does not exists"
fi

# or you can club both the condistions in single if statement as below
if [ -e "${file}" -a  -s "${file}" ]

then 
echo "File is present with some content"
else
echo "File is not present or no content in the file"

fi