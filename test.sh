#!/bin/bash
if [ $# -ne 1 ]
then

echo -e "Pass the arguments as below\n$0 <<start | stop | status | restart | install>>"
exit

fi

echo -e "start | stop | restart | install | remove | install | status " | grep -w $1 >>/dev/null

if [ $? -ne 0 ]

then
echo "then please arguments like below"
echo -e "START | STOP | RESTART | INSTALL | STATUS"

fi

if [ "$1" == "status" ]

then

 status=$(systemctl status httpd | grep active  | awk '{ print $2 }')
 
 echo "The status is $status"

fi 



if [ "$1" == "start" ]

then 

echo -e "starting the httpd" >>/dev/null
sudo systemctl start httpd
  if [ $? -ne 0 ]
  then
  echo -e "it failed, please check httpd service manually"
  else
  echo -e "httpd service started successfully"
  fi

fi


if [ "$1" = "stop" ]

then

echo -e "stopping the httpd" >>/dev/null
sudo systemctl stop httpd
  if [ $? -ne 0 ]
  then
  echo -e "it failed to stop, please check httpd service manually"
  else
  echo -e "httpd service stopped successfully"
  fi


fi


if [ "$1" = "restart" ]

then

echo -e "restarting the httpd" >>/dev/null
sudo systemctl restart httpd
     if [ $? -ne 0 ]
     then
     echo -e "it failed, please check httpd service manually"
     else
     echo -e "httpd service restarted successfully"
     fi
fi


if [ "$1" = "remove" ]

then
echo -e "removing the httpd" >>/dev/null
sudo yum remove httpd -y
     if [ $? -ne 0 ]
    then
    echo -e "it failed, please check httpd service manually"
    else
    echo -e "httpd service removed successfully"
    fi


fi


if [ "$1" = "install" ]

then

echo -e "install the httpd" >>/dev/null
sudo yum install httpd -y
     if [ $? -ne 0 ]
     then
     echo -e "it failed, please check httpd service manually"
     else
     echo -e "httpd service installed successfully"
     fi


fi