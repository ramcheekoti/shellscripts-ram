#!/bin/bash
read -p "Enter the package name for installation: " req_pkg
echo -e "Installing the ${req_pkg}"
sudo yum install ${req_pkg} -y
echo "${req_pkg} is installed"
